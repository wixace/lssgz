﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Fs.Libs.Controller
{
    public class SceneController : MonoBehaviour {

        public void UnloadScene(string scene)=> SceneManager.UnloadSceneAsync(scene);

        public void LoadScene(string scene)=> SceneManager.LoadSceneAsync(scene);

        public void LoadSceneAdditive(string scene) => SceneManager.LoadScene(scene,LoadSceneMode.Additive);

    }
}
