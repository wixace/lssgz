﻿using UnityEngine;

namespace Assets.Pigcya
{
	public class DontDestroy : MonoBehaviour {

		public static DontDestroy instance = null;

		void Awake() {
			if (instance == null) {
				instance = this;
			}
			else if (instance != this) {
				Destroy (gameObject);
			}
			DontDestroyOnLoad(this.gameObject);

		}
	}
}
