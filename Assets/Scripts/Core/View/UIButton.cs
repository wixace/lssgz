﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIButton : MonoBehaviour {
    
    protected Button Button;
    protected virtual void Awake() => Button = GetComponent<Button>();

    protected virtual void Start() => SetAction(() => {
        AudioManager.Instance.Sfx();
    });

    protected void SetAction(Action wtf) {
        if (wtf != null)
            Button.onClick.AddListener(() => {
                wtf();
            });
    }

}
