﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UITimer : MonoBehaviour {


	[SerializeField] private float _interval = 1f;
	[SerializeField] private int _minutes, _seconds;
	[SerializeField] private string format = "mm:ss";

	private int _totalTime;

	private DateTime _time;

	private Text _timerText;

	public UnityEvent OnTimesUp;

	void Awake() {
		_timerText = GetComponent<Text>();
	}

	void OnEnable() {
		StopAllCoroutines();
		_totalTime = _minutes * 60 + _seconds;
		_time = new DateTime().AddMinutes(_minutes).AddSeconds(_seconds);
		StartCoroutine(StartCountdown());
	}

	public void SetTime(int min, int sec = 0) {
		StopAllCoroutines();
		_totalTime = min * 60 + sec;
		_time = new DateTime().AddMinutes(min).AddSeconds(sec);

	}

	public void StartTimer() {
		StartCoroutine(StartCountdown());
	}

	IEnumerator StartCountdown() {
		while (_totalTime > 0) {
			_timerText.text = _time.ToString(format);
			yield return new WaitForSeconds(_interval);
			_time = _time.AddSeconds(-1);
			_totalTime--;
		}
		OnTimesUp?.Invoke();
	}

	void OnDisable() {
		StopAllCoroutines();
	}
}
