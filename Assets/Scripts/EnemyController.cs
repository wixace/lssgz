﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    const int StateIdle = 0;
    const int StateWalk = 1;
    const int StateAttack = 2;

    const float RandIdleTimeMin = 0.5f;
    const float RandIdleTimeMax = 1f;
    const float RandWalkTimeMin = 1.25f;
    const float RandWalkTimeMax = 2.25f;
    const float RandAttackTimeMin = 0.25f;
    const float RandAttackTimeMax = 2.1f;

    const int DefaultHp = 40;
    const int DefaultDmg = 5;
    const float MovementForce = 1.0f;
    const float MaxMoveVelocity = -1.0f;
    const string PlayerProjTag = "PlayerProjectile";

    private bool _isFirstState = true;
    private float _fireRate = 2.0f;
    private float _nextFire = 0.0f;
    private int _currentState;
    private int _hp = DefaultHp;
    private int _projectileDamage = DefaultDmg;
    public MoveDirection currentDirection = MoveDirection.Right;
    private Animator _animator;

    private Rigidbody2D _rb2d;
    public Rigidbody2D projectileLeft;
    public Rigidbody2D projectileRight;

    public enum MoveDirection
    {
        Left,
        Right
    }
    void Start()
    {
        _animator = GetComponent<Animator>();
        _rb2d = GetComponent<Rigidbody2D>();
        _rb2d.freezeRotation = true;

        StartCoroutine(BasicAi());
    }
    void Update()
    {
        if (_hp <= 0)
        {
            
            Destroy(gameObject);
            GameManager.Instance.AddCoin();
        }

        CheckPlayerLocation();

        if (_currentState == StateWalk && currentDirection == MoveDirection.Left)
        {
            if (_rb2d.velocity.x > MaxMoveVelocity)
                _rb2d.AddForce(Vector2.left * MovementForce, ForceMode2D.Impulse);
        }
        else if (_currentState == StateWalk && currentDirection == MoveDirection.Right)
        {
            if (_rb2d.velocity.x < Mathf.Abs(MaxMoveVelocity))
                _rb2d.AddForce(Vector2.right * MovementForce, ForceMode2D.Impulse);
        }

        if (_currentState == StateAttack && currentDirection == MoveDirection.Left)
        {
            if(Time.time > _nextFire)
            {
                _nextFire = Time.time + _fireRate;
                var proj = Instantiate(projectileLeft, transform.position + new Vector3(0, GetRandomYLocation(), 0), Quaternion.identity);
                proj.transform.localScale = new Vector3(4.5f, 4.5f);
            }
        }
        else if (_currentState == StateAttack && currentDirection == MoveDirection.Right)
        {
            if (Time.time > _nextFire)
            {
                _nextFire = Time.time + _fireRate;
                var proj = Instantiate(projectileRight, transform.position + new Vector3(0, GetRandomYLocation(), 0), Quaternion.identity);
                proj.transform.localScale = new Vector3(4.5f, 4.5f);
            }
        }
    }

    private float GetRandomYLocation()
    {
        var randomNo = Random.Range(0, 2);

        if (randomNo == 0)
            return -1.3f;
        else return 1.3f;
    }

    private float _waitBetweenActions = 0.1f;

    private IEnumerator BasicAi()
    {
        if(_isFirstState)
        {
            StartCoroutine(PerformWalkTask());
            _isFirstState = false;
        }

        for (; ; )
        {
            yield return new WaitForSeconds(_waitBetweenActions);

            var action = Random.Range(0, 3);

            switch (action)
            {
                case StateIdle:
                    StartCoroutine(PerformIdleTask());
                    break;

                case StateWalk:
                    StartCoroutine(PerformWalkTask());
                    break;

                case StateAttack:
                    StartCoroutine(PerformAttackTask());
                    break;
            }
        }
    }

    private IEnumerator PerformIdleTask()
    {
        _currentState = StateIdle;
        _animator.SetInteger("enemyState", _currentState = StateIdle);
        _waitBetweenActions = Random.Range(RandIdleTimeMin, RandIdleTimeMax);

        yield return new WaitForSeconds(_waitBetweenActions);
    }

    private IEnumerator PerformWalkTask()
    {
        _currentState = StateWalk;
        _animator.SetInteger("enemyState", _currentState = StateWalk);
        _waitBetweenActions = Random.Range(RandWalkTimeMin, RandWalkTimeMax);

        yield return new WaitForSeconds(_waitBetweenActions);
    }

    private IEnumerator PerformAttackTask()
    {
        _currentState = StateAttack;
        _animator.SetInteger("enemyState", _currentState = StateAttack);
        _waitBetweenActions = Random.Range(RandAttackTimeMin, RandAttackTimeMax);

        yield return new WaitForSeconds(_waitBetweenActions);
    }

    private void CheckPlayerLocation()
    {
        if (KenAnimator.CurrentPosition.x > transform.position.x)
        {
            if (currentDirection != MoveDirection.Right)
                ChangeDirection(MoveDirection.Right);
        }
        else
        {
            if (currentDirection != MoveDirection.Left)
                ChangeDirection(MoveDirection.Left);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<BoxCollider2D>().CompareTag(PlayerProjTag))
        {
            _hp -= Projectile.damage;
            if (Projectile.direction == "right")
                _rb2d.AddForce(new Vector2(2, 0), ForceMode2D.Impulse);
            else
                _rb2d.AddForce(new Vector2(-2, 0), ForceMode2D.Impulse);

        }
    }

    private void ChangeDirection(MoveDirection moveDir)
    {
        if (currentDirection != moveDir)
        {
            switch (moveDir)
            {
                case MoveDirection.Right:
                    transform.Rotate(0, -180, 0);
                    currentDirection = MoveDirection.Right;
                    break;

                case MoveDirection.Left:
                    transform.Rotate(0, 180, 0);
                    currentDirection = MoveDirection.Left;
                    break;
            }
        }
    }
}
