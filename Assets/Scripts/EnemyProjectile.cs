﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    const float ProjVelocityMin = 6.0f;
    const float ProjVelocityMax = 12.0f;
    const float ProjBaseDamage = 5.0f;

    public GameObject projectileExplosion;
    private Rigidbody2D _rb;
    public static float Damage;
    public float velocityX = 0.0f;
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();

        velocityX = Random.Range(ProjVelocityMin, ProjVelocityMax);
        SetMovementDirection();

        Damage = ProjBaseDamage;
    }
    void Update()
    {
        _rb.velocity = new Vector2(velocityX, 0);
    }

    private void SetMovementDirection()
    {
        if(GameObject.Find("gy").transform.position.x > transform.position.x)
        {

        }
        else
        { 
            velocityX = -velocityX;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            projectileExplosion.transform.localScale = new Vector3(8, 8, 0);
            GameObject objClone = Instantiate(projectileExplosion, transform.position, Quaternion.identity);
            Destroy(objClone, 0.45f);
            Destroy(gameObject);
        }
    }
}
