﻿using UnityEngine.UI;

public class GameManager : USingleton<GameManager>
{
   public Text CoinText, PotionText, YBText, RockText;

   public int Coin, Potion, YB, Rock;
   private void Start()
   {
      UpdateText();
   }

   private void UpdateText()
   {
      CoinText.text = Coin.ToString();
      PotionText.text = Potion.ToString();
      YBText.text = YB.ToString();
      RockText.text = Rock.ToString();
   }
   
   public void AddCoin()
   {
      Coin += 4;
      Potion += 3;
      YB += 2;
      Rock += 1;
      UpdateText();
   }
   
}
