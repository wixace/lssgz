﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputHandler : MonoBehaviour
{

    public Button Btn;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<InputField>().onValueChanged.AddListener(x =>
        {
            Btn.interactable = x.Length > 0;
            Intent.Name = x;
        });
    }

 
}
