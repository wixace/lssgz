﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KenAnimator : MonoBehaviour
{
    const int StateIdle = 0;
    const int StateWalk = 1;
    const int StateCrouch = 2;
    const int StateJump = 3;
    const int StateAttack = 4;
    private int _currentState;

    const int WalkSpeed = 8;
    const int MovementForce = 6;
    const int MaxMoveVelocity = -5;
    const int JumpForce = 80;

    private int _dynamicMoveForce = MovementForce;
    private bool _isInAir = false;
    private float _fireRate = 0.917f;
    private float _nextFire = 0.0f;

    private int _hp = 100;

    private MoveDirection _currentDirection = MoveDirection.Left;
    private Rigidbody2D _rb;
    public Rigidbody2D projectileRight;
    public Rigidbody2D projectileLeft;
    private Animator _animator = new Animator();

    private BoxCollider2D _boxCollider;
    private float _boxColCrouchHeight;
    private float _boxColNormalHeight;
    private float _boxColWidth;

    public static Vector3 CurrentPosition;

    enum MoveDirection
    {
        Left,
        Right
    }

     public void Attack() {
        _attack = true;
    }

    bool _moveLeft, _moveRight,_attack,_isJump,_isCrouch;

    public void Jump() {
        _isJump = true;
    }

    public void Crouch() {
        _isCrouch = true;
    }


    public void MoveLeft() {
        _moveLeft =true;
    }

    public void MoveRight() {
        _moveRight = true;
    }

    public void Idle() {
        _moveLeft = _moveRight=_attack=false;
        _animator.SetInteger("state", _currentState = 0);
    }
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();

        _boxCollider = GetComponent<BoxCollider2D>();
        _boxColNormalHeight = _boxCollider.size.y;
        _boxColWidth = _boxCollider.size.x;
        _boxColCrouchHeight = _boxColNormalHeight * 0.65f;
    }
    void Update()
    {
        CurrentPosition = transform.position;

        if (_animator.GetInteger("state") != 0)
            _animator.SetInteger("state", _currentState = StateIdle);

        if (Input.GetKey(KeyCode.A) || _moveLeft)
        {
            ChangeDirection(MoveDirection.Left);

            if (_rb.velocity.x > MaxMoveVelocity)
                _rb.AddForce(Vector2.left * _dynamicMoveForce, ForceMode2D.Impulse);

            _animator.SetInteger("state", _currentState = StateWalk);
        }

        if (Input.GetKey(KeyCode.D) || _moveRight)
        {
            ChangeDirection(MoveDirection.Right);

            if (_rb.velocity.x < Mathf.Abs(MaxMoveVelocity))
                _rb.AddForce(Vector2.right * _dynamicMoveForce, ForceMode2D.Impulse);

            _animator.SetInteger("state", _currentState = StateWalk);
        }

        if (Input.GetKeyDown(KeyCode.W) || _isJump)
            if (!_isInAir)
            {
                _rb.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
                _animator.SetInteger("state", _currentState = StateJump);
                Instantiate(projectileRight, transform.position + new Vector3(3, 1), Quaternion.identity).transform.localScale = new Vector3(0.5f, 0.5f, 0);
                Instantiate(projectileRight, transform.position + new Vector3(3, -0.5f), Quaternion.identity).transform.localScale = new Vector3(0.5f, 0.5f, 0);
                Instantiate(projectileRight, transform.position + new Vector3(3, 1.5f), Quaternion.identity).transform.localScale = new Vector3(0.5f, 0.5f, 0);
                Instantiate(projectileLeft, transform.position + new Vector3(-3, -0.5f), Quaternion.identity).transform.localScale = new Vector3(0.5f, 0.5f, 0);
                Instantiate(projectileLeft, transform.position + new Vector3(-3, 1), Quaternion.identity).transform.localScale = new Vector3(0.5f, 0.5f, 0);
                Instantiate(projectileLeft, transform.position + new Vector3(-3, 1.5f), Quaternion.identity).transform.localScale = new Vector3(0.5f, 0.5f, 0);
                _isJump = false;
            }

        if (Input.GetKey(KeyCode.S)||_isCrouch)
        {
            _animator.SetInteger("state", _currentState = StateCrouch);
        }

        if (Input.GetKey(KeyCode.Space) || _attack)
        {
            _animator.SetInteger("state", _currentState = StateAttack);

            if (_currentDirection == MoveDirection.Right) {
                if (Time.time > _nextFire) {
                    _nextFire = Time.time + _fireRate;
                    Instantiate(projectileRight, transform.position + new Vector3(3, 1), Quaternion.identity).transform.localScale = new Vector3(0.5f, 0.5f, 0);
                }
            } else {
                if (Time.time > _nextFire) {
                    _nextFire = Time.time + _fireRate;
                    Instantiate(projectileLeft, transform.position + new Vector3(-3, 1), Quaternion.identity).transform.localScale = new Vector3(0.5f, 0.5f, 0);

                }
            }
        }

        CheckIfAttackOrCrouch();
    }

    private void ChangeDirection(MoveDirection moveDir)
    {
        if (_currentDirection != moveDir)
        {
            switch (moveDir)
            {
                case MoveDirection.Right:
                    transform.Rotate(0, -180, 0);
                    _currentDirection = MoveDirection.Right;
                    break;

                case MoveDirection.Left:
                    transform.Rotate(0, 180, 0);
                    _currentDirection = MoveDirection.Left;
                    break;
            }
        }
    }

    private void CheckIfAttackOrCrouch()
    {
        if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack") ||
            _animator.GetCurrentAnimatorStateInfo(0).IsName("Crouch_simple"))
            _dynamicMoveForce = 0;
        else
            _dynamicMoveForce = MovementForce;

        if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Crouch_simple"))
            _boxCollider.size = new Vector2(_boxColWidth, _boxColCrouchHeight);
        else
            _boxCollider.size = new Vector2(_boxColWidth, _boxColNormalHeight);
    }

    private void SetBoxColliderHeight()
    {
        if (_currentState == StateCrouch)
            _boxCollider.size = new Vector2(_boxColWidth, _boxColCrouchHeight);
        else
            _boxCollider.size = new Vector2(_boxColWidth, _boxColNormalHeight);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        _isInAir = false;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        _isInAir = true;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {

        _isInAir = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "EnemyProjectile")
        {
            _hp -= (int)EnemyProjectile.Damage;
          //Debug.Log("HP: " + HP);
            GameObject hpBar = GameObject.Find("Bar");
            hpBar.transform.localScale = new Vector3((float)_hp / 100.0f, 1.0f);
        }
    }
}
