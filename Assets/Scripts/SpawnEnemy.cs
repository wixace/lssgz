﻿using System.Collections;
using System.Collections.Generic;
using Giant.PG;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    private const float SpawnStartDelayMin = 0.5f;
    private const float SpawnStartDelayMax = 5.0f;
    private const float SpawnWaitMin = 8.0f;
    private const float SpawnWaitMax = 16.5f;

    public GameController cacm;
    public static string SpawnerName;

    public GameObject enemy;
    private bool _isFirstSpawn = true;


    // Start is called before the first frame update
    void Start()
    {
       cacm = new GameController();
       SpawnerName = this.name+cacm.ToString();
       GameController.Init();
        StartCoroutine(SpawnEnemyAction());
    }

    private IEnumerator SpawnEnemyAction()
    {
        for (; ; )
            if (_isFirstSpawn)
            {
                var randTime = Random.Range(SpawnStartDelayMin, SpawnStartDelayMax);
                yield return new WaitForSeconds(randTime);
                _isFirstSpawn = false;
                InstantiateEnemy();
            }
            else
            {
                var randTime = Random.Range(SpawnWaitMin, SpawnWaitMax);
                yield return new WaitForSeconds(randTime);
                InstantiateEnemy();
            }
    }

    private void InstantiateEnemy()
    {
        Instantiate(enemy, transform.position, new Quaternion(0, 180, 0, 0));
    }
}
