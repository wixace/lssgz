﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UICountdownTimer : MonoBehaviour {


    [SerializeField] private float _interval = 1f;
    [SerializeField] private int _minutes, _seconds;
    private int _totalTime;

    private DateTime _time;

    private Text _timerText;

    [FormerlySerializedAs("OnTimesUp")] public UnityEvent onTimesUp;

    void Awake() {
        _timerText = GetComponent<Text>();
    }

    void OnEnable() {
        StopAllCoroutines();
        _totalTime = _minutes * 60 + _seconds;
        _time = new DateTime().AddMinutes(_minutes).AddSeconds(_seconds);
        StartCoroutine(startt());
    }

    public void SetTime(int min, int sec = 0) {
        StopAllCoroutines();
        _totalTime = min * 60 + sec;
        print(_totalTime);
        _time = new DateTime().AddMinutes(min).AddSeconds(sec);
        StartCoroutine(startt());
    }


    IEnumerator startt() {
        while (_totalTime > 0) {
            _timerText.text = _time.ToString("mm:ss");
            yield return new WaitForSeconds(_interval);
            _time = _time.AddSeconds(-1);
            _totalTime--;
        }
        onTimesUp?.Invoke();


    }

    void OnDisable() {
        StopAllCoroutines();
    }
}
