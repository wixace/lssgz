﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class WKWebView : USingleton<WKWebView> {
	
	WebViewObject webViewObject;

	public static string Url { get; set; }
		
	void Start() {
		webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
		webViewObject.Init(
		                   cb: (msg) => { Debug.Log(string.Format("CallFromJS[{0}]", msg)); },
		                   err: (msg) => { Debug.Log(string.Format("CallOnError[{0}]", msg)); },
		                   started: (msg) => { Debug.Log(string.Format("CallOnStarted[{0}]", msg)); },
		                   ld: (msg) => {
			                   webViewObject.SetVisibility(true);
		                   },enableWKWebView:true,transparent:true);
		webViewObject.LoadURL(Url.Replace(" ", "%20"));
		webViewObject.SetVisibility(true);
	}
	
	
}