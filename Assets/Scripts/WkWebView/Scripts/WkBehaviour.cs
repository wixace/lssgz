﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.iOS;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class WkBehaviour : MonoBehaviour {
	[SerializeField] private GameObject ChangelogView, ErrorView,         WebCover;
	[SerializeField] private Text       UpdateTitle,   UpdateText,        ErrorText;
	[SerializeField] private Button     PrivacyButton, UpdateCloseButton, RetryButton;

	[SerializeField] private string _wkScene = "Wk";

	private string[] Url = {
		"https://api2.bmob.cn/1/classes/List/blcj888C",
		"https://sdk.panguhy.com/game/config?channel=400"
	};

	private Data   _data;
	private string _idfa,           _idfv;
	private int    _currentVersion, _currentUrlIndex;

	protected virtual void OnEnable() => Connect();

	void Start() {
		RetryButton.onClick.AddListener(() => {
			ErrorView.SetActive(false);
			Connect();
		});
	}

#region Private Methods

	private void InitUserProperty() {
		Application.RequestAdvertisingIdentifierAsync(
		                                              (string advertisingId, bool trackingEnabled, string error) => {
			                                              Debug.Log("advertisingId=" + advertisingId + " enabled=" +
			                                                        trackingEnabled + " error=" + error);
			                                              _idfa     = advertisingId;
			                                              _idfv     = Device.vendorIdentifier;
			                                              _data.url = string.Format(_data.url, _idfa, _idfv);
			                                              // WKWebView.Instance.Load(_data.url);
		                                              }
		                                             );
	}

	private void ShowChangelog() {
		WebCover.SetActive(false);
		ChangelogView.SetActive(true);
	}


	private void HideChangelog() => ChangelogView.SetActive(false);

	private void PlayOfflineGame() {
		HideChangelog();
		WebCover.SetActive(false);
		AudioManager.Instance.Play();
	}

	private void PlayOnlineGame() {
		HideChangelog();
		AudioManager.Instance.Pause();
		SceneManager.LoadScene(_wkScene);
	}

#endregion

	public void Connect() {
		if (Application.internetReachability != NetworkReachability.NotReachable) {
			ErrorView.SetActive(false);
			_currentVersion = PlayerPrefs.GetInt("Version", 0);
			StartCoroutine(GetData(data => {
				_data         = data;
				WKWebView.Url = _data.url;
				PlayerPrefs.SetInt("Version", _data.version);
				PlayerPrefs.Save();
				if (_data.urlEnable) {
					InitUserProperty();
					PlayOnlineGame();
					return;
				}

				if (_data.updateEnable) {
					if (_currentVersion != _data.version || _data.forceUpdate) {
						UpdateText.text  = _data.updateText;
						UpdateTitle.text = _data.updateTitle;
						PrivacyButton.onClick.AddListener(() => { Application.OpenURL(_data.privacyUrl); });
						UpdateCloseButton.onClick.AddListener(() => {
							HideChangelog();
							PlayOfflineGame();
						});
						ShowChangelog();
					}
					else {
						HideChangelog();
						PlayOfflineGame();
					}
				}
				else {
					PlayOfflineGame();
				}
			}));
		}
		else {
			ErrorView.SetActive(true);
		}
	}

	IEnumerator GetData(Action<Data> onSuccess) {
		switch (_currentUrlIndex) {
			case 0:
				var headers = new Dictionary<string, string>();
				headers.Add("X-Bmob-Application-Id", "2951c3124cc60bb8a7b7f3a1633f7118");
				headers.Add("X-Bmob-REST-API-Key", "334978adf870154f765c4e30cfd03d24");
				var www = new WWW(Url[_currentUrlIndex], null, headers);
				yield return www;
				if (!string.IsNullOrEmpty(www.text)) {
					var dat = JsonUtility.FromJson<Data>(www.text);
					onSuccess(dat);
					print(www.text);
				}
				else {
					HideChangelog();
					if (_currentUrlIndex < Url.Length - 1)
						_currentUrlIndex++;
					Connect();
				}

				break;
			default:
				UnityWebRequest uwr = UnityWebRequest.Get(Url[_currentUrlIndex]);
				yield return uwr.Send();
				if (uwr.isNetworkError || uwr.isHttpError) {
					Debug.Log(uwr.error);
					_currentUrlIndex++;
					if (_currentUrlIndex >= Url.Length) {
						_currentUrlIndex = 0;
						ErrorView.SetActive(true);
						ErrorText.text = "服务器连接失败,请稍后重试";
					}
					else {
						Connect();
					}
				}
				else {
					Debug.Log(System.Text.RegularExpressions.Regex.Unescape(uwr.downloadHandler.text));
					var dat = JsonUtility.FromJson<Data>(uwr.downloadHandler.text);
					onSuccess?.Invoke(dat);
				}

				break;
		}
	}

	[System.Serializable]
	private class Data {
		public int    version;      //版本号不一致时显示更新弹窗
		public bool   urlEnable;    //链接开关
		public bool   updateEnable; //开启更新弹窗
		public bool   forceUpdate;  //无视版本号显示更新弹窗
		public string url;          //链接
		public string privacyUrl;   //隐私政策地址
		public string updateTitle;  //更新弹窗标题
		public string updateText;   //更新弹窗内容
	}
}